# hbo-ict-client-03

Run instructions
```
git clone https://gitlab.com/ekkebus/hbo-ict-client-03.git
```

# Setup NodeJS op je eigen omgeving
1. Download en installeer NodeJS vanaf https://nodejs.org/en/ (de LST versie)
    - Node JS heeft een mooie package manager (NPM) welke wij gaan gebruiken
1. Navigeer naar de projectfolder waar je het NPM project wilt aanmaken
1. Maak een package.json bestand met onderstaand commando
```
npm init
```
4. Installeer de benodigde node modules (libaries)
```
npm install --global gulp-cli 	
npm install --save-dev gulp	
npm install --save-dev gulp-autoprefixer
npm install --save-dev gulp-clean-css
npm install --save-dev gulp-concat
```

of (zelfde resultaat)

```
npm install --global gulp-cli
npm install --save-dev gulp gulp-autoprefixer gulp-clean-css gulp-concat
```

# Uitvoeren van een Gulp taak
```
gulp build
```

# Toubleshooting
## Oplossen van 'UnauhtorizedAccess' in Windows
Als je onderstaande melding krijgt bij het uitvoeren van gulp.

```
gulp : File C:\Users\xyz\AppData\Roaming\npm\gulp.ps1 cannot be loaded because running
scripts is disabled on this system. 
For more information, see about_Execution_Policies at 
https:/go.microsoft.com/fwlink/?LinkID=135170.
At line:1 char:1
+ gulp build
+ ~~~~
+ CategoryInfo          : SecurityError: (:) [], PSSecurityException
+ FullyQualifiedErrorId : UnauthorizedAccess
```

Voer dan onderstaand commando uit als Administrator van je systeem, en geef via de commandline toestemming.

```
Set-ExecutionPolicy RemoteSigned 
```
    
## NPM werkt niet goed
Is node goed geïnstalleerd? Controleer via onderstaande commando de versie.
```
node -v
```
## Gulp werkt niet goed
Is node gulp (goed) geïnstalleerd? Controleer via onderstaande commando de versie.
```
gulp -v
```
Heeft Gulp de juiste versie in mijn development omgeving? Als je 1.0.0 hebt:
```
npm update gulp --save-dev
```
    
## Ik krijg Veel en vage gulp errors? Gooi de node_modules folder weg of
```
npm rebuild gulp-sass --force
```
