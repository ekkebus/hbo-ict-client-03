//gulpfile.js
//read: https://gulpjs.com/docs/en/getting-started/async-completion

//imports die je nodig hebt in de onderstaande delen van je Gulp file
const gulp = require('gulp');
const {series} = require('gulp');
//const browserSync = require('browser-sync').create();

//onderstaand gulp task is uit te voeren met 'gulp build'
gulp.task('build', function(done){
   console.log('Running build task');
   done();  //callback function, die aangeeft dat build klaar is
});

//onderstaand gulp task is uit te voeren met 'gulp build'
gulp.task('default', function(done){
   console.log('Running default task');
   done();  //resolve the promiss
});

//taken geven een promise terug
gulp.task('read-html-files', function(){
   return gulp.src('**/*.html')
        .pipe(gulp.dest('dist'));

   //gulp.src('**/*.html')       leest alle bestanden uit die aan de machter voldoen
   //.pipe()	                  geeft de inputstream door aan de volgende gulp functie
   //gulp.dest('dist')           schrijft de output stream weg naar een folder

});

//of beter schrijf een constante functie (een sub functie) die een promisse returned (zie voorbeeld) of gebruik een callback functie
//die je later export (zie regel 49)
const html = () => {
   return gulp.src('index.html')
        .pipe(gulp.dest('dist'));
};

const css = () => {
   return gulp.src('./src/css/**/*.css')
        .pipe(gulp.dest('dist'));
};

// je kunt taken achter elkaar https://gulpjs.com/docs/en/api/series 
// of 
// paralell https://gulpjs.com/docs/en/api/parallel/ laten uitvoeren.
// series() en parallel() hebben meerdere functies als input
exports.release = series(html, css);

//TODO "Maak een taak 'js' die de javascriptbestanden in de dist map zet. En voeg deze toe aan de release taak, zodat deze wordt uitgevoerd via 'gulp release'" 



// browser sync, geen LiveServer meer nodig :)
// install via npm first, uncomment vanaf gulp.task(
// npm install --save-dev browser-sync 
// gulp.task('serve', function() {

//    //root folder die de locale server gaat serveren
//    browserSync.init({
//        server: "./dist/"
//    });

//    gulp.watch("./src/css/**.css", series(css));
//    //TODO voeg de javascript taak toe
//    gulp.watch("./index.html", series(html));
//    //als er iets wijzigt in de dist map
//    gulp.watch("./dist/**/*.*").on('change', browserSync.reload);
// });
