//example 1
const example1 = (() =>{
    let _message = 'Example 1';
    
    var logItem = document.createElement('p');
    logItem.innerHTML = _message;
    document.getElementById('console').appendChild(logItem);

    return {
        message : _message 
    }
})();