//example 2
const example2 = (() =>{
    let _message = 'Example 2';

    var logItem = document.createElement('p');
    logItem.innerHTML = _message;
    document.getElementById('console').appendChild(logItem);
    
    return {
        message : _message 
    }
})();